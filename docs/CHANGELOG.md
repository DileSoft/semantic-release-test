# [0.7.0](https://gitlab.com/DileSoft/semantic-release-test/compare/v0.6.0...v0.7.0) (2023-12-15)


### Features

* new version ([d522064](https://gitlab.com/DileSoft/semantic-release-test/commit/d5220640f8462c76e24883514f02f59f967afa3e))

# [0.6.0](https://gitlab.com/DileSoft/semantic-release-test/compare/v0.5.0...v0.6.0) (2023-12-15)


### Features

* new version ([16e8fd2](https://gitlab.com/DileSoft/semantic-release-test/commit/16e8fd2d4394f9846d94b3c7c39fccff31cdb28e))

# [0.5.0](https://gitlab.com/DileSoft/semantic-release-test/compare/v0.4.0...v0.5.0) (2023-12-15)


### Features

* new version ([69a69c6](https://gitlab.com/DileSoft/semantic-release-test/commit/69a69c64176ac7b5e5b6e8e2f70c07cde6f9d9b9))

# [0.4.0](https://gitlab.com/DileSoft/semantic-release-test/compare/v0.3.0...v0.4.0) (2023-12-15)


### Features

* new version ([f6cb709](https://gitlab.com/DileSoft/semantic-release-test/commit/f6cb7099ceccb22bced3b464a0e21db4d7d14912))
* new version ([7a50960](https://gitlab.com/DileSoft/semantic-release-test/commit/7a5096059cfbfaf167792fe24ccf720fead6f464))

# [0.3.0](https://gitlab.com/DileSoft/semantic-release-test/compare/v0.2.0...v0.3.0) (2023-12-15)


### Features

* new version ([c3cdefd](https://gitlab.com/DileSoft/semantic-release-test/commit/c3cdefd55415b256f4e492d5cc79834771d8719d))

# [0.2.0](https://gitlab.com/DileSoft/semantic-release-test/compare/v0.1.4...v0.2.0) (2023-12-15)


### Features

* new version ([8c6b3cf](https://gitlab.com/DileSoft/semantic-release-test/commit/8c6b3cf0b1c4ca893ab9b6aed58e485cab8dd7fa))

## [0.1.4](https://gitlab.com/DileSoft/semantic-release-test/compare/v0.1.3...v0.1.4) (2023-07-19)


### Bug Fixes

* changes ([c67f12c](https://gitlab.com/DileSoft/semantic-release-test/commit/c67f12c4aba7add95749406d67e80265366ac02f))

## [0.1.3](https://gitlab.com/DileSoft/semantic-release-test/compare/v0.1.2...v0.1.3) (2023-07-19)


### Bug Fixes

* changes log ([03cc7b2](https://gitlab.com/DileSoft/semantic-release-test/commit/03cc7b2afe4e80fe24d8637118e2cf42828e0ad4))
