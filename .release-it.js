module.exports = {
    gitlab: {
        release: true,
    },
    npm: {
        publish: false,
    },
    plugins: {
        "@release-it/conventional-changelog": {
          "preset": {
            "name": "angular"
          },
          "infile": "docs/CHANGELOG.md"
        }
      }
}